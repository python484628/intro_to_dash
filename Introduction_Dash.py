
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# October 2021

############## Import Libraries
import pandas as pd
import dash
from dash import dcc
from dash import html
print(dcc.__version__)





#################################################### First example of a dashboard

# Create a dashboard (empty)
app = dash.Dash("test")

# We create a layout 
app.layout = html.Div()

# If debug True, tools to see errors, interactions, etc
if __name__ =='__main__':
    app.run_server(debug=False)

# Then copy the link in console  "Dash is running on http://........."   and paste it on an internet software (chrome, mozilla etc)
# Then to stop the application you have to stop it in your console 

####################################################################   
    



#################################################### Second example of a dashboard
  
  
# Print hello dash dash on the screen
app.layout = html.Div([
    html.H1('Hello Dash')])
if __name__ =='__main__':
    app.run_server(debug=False)

####################################################################   







#################################################### Third example of a dashboard
 


# Add another sentence on the screen thanks to html.Div
app.layout = html.Div([html.H1('Hello Dash'), html.Div('Dash : A web application framework for Python.')])
    
if __name__ =='__main__':
    app.run_server(debug=False)
####################################################################   
    
   
    
   
    
   
    

#################################################### Fourth example of a dashboard
    
    
# Add an empty graph after the title and the sentence
app.layout = html.Div([html.H1('Hello Dash'),
                       html.Div('Dash : A web application framework for Python.'),
                       dcc.Graph()])
    
if __name__ =='__main__':
    app.run_server(debug=False)

####################################################################  
    
    






#################################################### Fifth example of a dashboard

# Fill the graph (bar plot) with manual data under the title and sentence 
app.layout = html.Div([
    html.H1('Hello Dash'),
    html.Div('Dash : A web application framework for Python.'),
    # We give data in a dictionnary (x and y values, type of graph, name of graph)
    dcc.Graph(id = 'example-graph', figure={'data':[{'x': [1,2,3], 'y':[4,1,2], 'type':'bar', 'name': 'SF'}],})])

if __name__ =='__main__':
    app.run_server(debug=False)
    
####################################################################  










#################################################### Sixth example of a dashboard

# Add title to our bar plot and a second variable B with the same values as A 
app.layout = html.Div([
    html.H1('Hello Dash'),
    html.Div('Dash : A web application framework for Python.'),
    dcc.Graph(id = 'example-graph', figure={'data':[{'x': [1,2,3], 'y':[4,1,2], 'type':'bar', 'name': 'A'},
                                                    {'x': [1,2,3], 'y':[4,1,2], 'type':'bar', 'name': 'B'}
                                                    ],
                                            'layout':{
                                                'title': 'Dash Data Visualisation'
                                                }
                                            })
                                            ])

if __name__ =='__main__':
    app.run_server(debug=False)

####################################################################  




     




#################################################### Seventh example of a dashboard                              
# Open data 
df = pd.read_csv('http://java.transfert.free.fr/data2011.csv')

# Create function to create a table from our dataframe 
def generate_table(dataframe, max_rows=10):
    return html.Table(
    #Header
    [html.Tr([html.Th(col) for col in dataframe.columns])] +
    
    # Body
    [html.Tr([
        html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )

# Create the app, add title with html.H4 and use the function generate_table() to create a table from our dataframe df
app = dash.Dash("test") 
app.layout = html.Div(children = [
    html.H4(children='US Agriculture Exports (2011)'),
    generate_table(df)
    ])                                       

# Run the app where you will see the table
if __name__ =='__main__':
    app.run_server(debug=False) 























