# Introduction_Dash

In this py file you can have a look at 7 different dashboards created with dash library. It's a good introduction when you're a beginner to discover and understand how dash works. In this tutorial you don't need any data, everything is created manually or retrieved from a website. 


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

October 2021
